#version 430


// A way to make a rectangle without branching
vec2 lut[4] = 
{
	vec2(0, 0),
	vec2(0, 1),
	vec2(1, 1),
	vec2(1, 0),
};

struct ssbo_data
{
	float x;
	float y;

	// Unused dx and dy variables
	// Don't need this anymore if we're moving the delta
	// into a separate struct in the C code
	// float pad[2];

	// Got rid of the spritenum as well
	// Instead we calculate it based on data_index
	// uint spritenum;
};

layout(std430, binding = 1) buffer ssbo
{
	ssbo_data SsboData[];
};


vec2 Rotate(vec2 p, float deg)
{
	float rad = radians(deg);

	float s = sin(rad);
	float c = cos(rad);

	float x = c * p.x + -s * p.y;
	float y = s * p.x + c * p.y;
	
	return vec2(x, y);
}

vec2 Scale(float scale, vec2 p)
{
	return p * scale;
}

out vec2 vf_TexCoords;
out float vf_Idx;

// Copied from the C code
#define TEXTURE_WIDTH 430.0
#define TEX_U (36.0 / TEXTURE_WIDTH)
#define TEX_U_RIGHT (32.0 / TEXTURE_WIDTH)
#define BUNNY_SPRITE_COUNT 12
#define BUNNY_WIDTH 32
#define BUNNY_HEIGHT 32

#define ARENA_WIDTH 1200
#define ARENA_HEIGHT 1200


#define BUNNY_WIDTH 32
#define TEX_WIDTH 430.0

// Y component must be negative, so that y=0 is the top left
uniform vec2 ortho = vec2(2.0/ARENA_WIDTH, -2.0/ARENA_HEIGHT);

uniform float scale = 1;


void main()
{
	int id = gl_VertexID;
	int data_index = id / 4;
	int lut_index = id % 4;

#if 0
	float x = 100;
	float y = 500;
	float w = 50;
	float h = 50;
#else
	// data_index = 0;
	ssbo_data Data = SsboData[data_index];
	float x = Data.x;
	float y = Data.y;
	float w = 32;
	float h = 32;
#endif

	// Calculate position
	float dx = lut[lut_index].x * w;
	float dy = lut[lut_index].y * h;
	
	// Rotate
	// Scale
	vec2 pos_model = vec2(dx, dy);
	vec2 half_model = vec2(w / 2.0, h / 2);
	
	// Uncomment to enable rotation
	// Our quad origin is at the top left, 
	// so we have to subtract half the model dimensions to rotate about the origin
	// pos_model -= half_model;
	// pos_model = Rotate(pos_model, float(data_index % 360));
	// pos_model += half_model;
	
	pos_model *= scale;

	vec2 pos_world = vec2(x, y) + pos_model;
	vec2 pos = (pos_world * ortho) - vec2(1.0, -1.0);

	// We want y = 0 to be the top left
	// Note: Negating the y compoent above in vec2(1.0, -1.0) achieves the same thing
	// pos.y = -pos.y;
	
	gl_Position = vec4(pos, 0, 1);

	// Calculate texture coords
	// We could hardcode a UV lookup table to avoid these multiplications
	vec2 uv;
	float u_delta = TEX_U_RIGHT * lut[lut_index].x;
	float v_delta = lut[lut_index].y;
	// uv.x = float(Data.spritenum) * TEX_U + u_delta;
	// We could just use the bunny number as a sprite index!
	uv.x = float((data_index / 1000) % BUNNY_SPRITE_COUNT) * TEX_U + u_delta;
	uv.y = 0 + v_delta;

	vf_TexCoords = uv;
	vf_Idx = float(data_index);
}
