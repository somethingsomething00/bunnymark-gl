// NAME: gui.c
// TIME OF CREATION: 2023-01-19 21:36:16
// AUTHOR:
// DESCRIPTION: Immediate GUI library by Z
// 				The library is not complete and in its current state, was only written as a supplement to the Bunnymark GL project
// 				Nevertheless, it has a good basis for future development
// 
// 				Uses a top down coordinate system (y=0 is the top of the screen)
// 				Currently uses OpenGL as a rendering backend

#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>


#include <GL/gl.h>

#define GUI_PI 3.141592654


#define GUI_MIN(a, b) ((a) < (b)) ? (a) : (b)
#define GUI_MAX(a, b) ((a) > (b)) ? (a) : (b)
#define GUI_CLAMP(have, mi, ma) GUI_MIN(GUI_MAX((have), (mi)), (ma))
#define GUI_ZERO_STRUCT(s) memset(&(s), 0, sizeof(s))
#define GUI_ARRAY_COUNT(a) (sizeof(a) / sizeof(a)[0])
#define GUI_COLOR3F_TO_COMPONENTS(c) c.r, c.g, c.b
#define GUI_COLOR3F(_r, _g, _b) (gui_color3f){.r = (_r), .g = (_g), .b = (_b)}

typedef struct
{
	int ortho_w;
	int ortho_h;

	int window_w;
	int window_h;

	float mouse_x;
	float mouse_y;

	int lmb_pressed;
	int rmb_pressed;

	bool element_is_active;
	int active_elements;

} gui_context;

typedef struct
{
	float min;
	float max;

	float value;
	float default_value;


	// Internal
	bool active;
	bool hot;
	bool hover;
	float mouse_x_on_press;
	float value_on_press;
	float transient_value;
	float mouse_dx;

} gui_slider;

typedef struct
{
	float x;
	float y;
} gui_v2f;


typedef struct
{
	float r;
	float g;
	float b;
} gui_color3f;

typedef struct
{
	float top;
	float bottom;
	float left;
	float right;
} gui_rect;

typedef enum
{
	GRE_RECT,
	GRE_CIRCLE,
} gui_render_entry_type;

typedef struct
{
	gui_render_entry_type Type;
	float x;
	float y;
	float w;
	float h;
	float radius;
	float rotation;
	
	gui_color3f color;
	float alpha;

} gui_render_entry;

typedef struct
{
	gui_render_entry Entries[512];
	unsigned int count;
} gui_render_group; 

typedef struct
{
	float s;
	float c;
} gui_sincos;



/**********************************
* Globals
**********************************/
static gui_sincos _GUI_SINCOS_LUT[360];
static gui_render_group _GUI_RENDER_GROUP;



void _GUI_EntryPush(gui_render_group *Group, gui_render_entry *Entry)
{
	assert(Group->count < GUI_ARRAY_COUNT(Group->Entries));
	Group->Entries[Group->count] = *Entry;
	Group->count++;
}

void _GUI_EntryPushRect(float x, float y, float w, float h, gui_color3f color, float alpha)
{
	gui_render_entry Entry = {0};
	Entry.Type = GRE_RECT;

	Entry.x = x;
	Entry.y = y;
	Entry.w = w;
	Entry.h = h;
	Entry.color = color;
	Entry.alpha = alpha;

	_GUI_EntryPush(&_GUI_RENDER_GROUP, &Entry);
}

void _GUI_EntryPushCircle(float x, float y, float radius, gui_color3f color, float alpha)
{
	gui_render_entry Entry = {0};
	Entry.Type = GRE_CIRCLE;

	Entry.x = x;
	Entry.y = y;
	Entry.radius = radius;
	Entry.color = color;
	Entry.alpha = alpha;

	_GUI_EntryPush(&_GUI_RENDER_GROUP, &Entry);
}



float _GUI_Radians(float degrees)
{
	return degrees * (GUI_PI / 180.0);
}

bool _GUI_PointIsInsideRect(gui_v2f Point, gui_rect *Rect)
{
	return ((Point.x >= Rect->left && Point.x <= Rect->right)
			&& (Point.y >= Rect->top && Point.y <= Rect->bottom));
}

void _GUI_Rect(float x, float y, float w, float h, float r, float g, float b, float a)
{
	float x0 = x;
	float x1 = x + w;
	float y0 = y;
	float y1 = y + h;

	glColor4f(r, g, b, a);
	
	glBegin(GL_QUADS);

	glVertex2f(x0, y0);
	glVertex2f(x0, y1);
	glVertex2f(x1, y1);
	glVertex2f(x1, y0);

	glEnd();
}

void _GUI_InitSincos()
{
	for(int i = 0; i < 360; i++)
	{
		float rad = _GUI_Radians(i);
		_GUI_SINCOS_LUT[i].s = sin(rad);
		_GUI_SINCOS_LUT[i].c = cos(rad);
	}
}

void GUI_Init(gui_context *Context, int ortho_w, int ortho_h)
{
	GUI_ZERO_STRUCT(*Context);
	Context->ortho_w = ortho_w;
	Context->ortho_h = ortho_h;

	_GUI_InitSincos();
}

void _GUI_Circle(float x, float y, float radius, float r, float g, float b)
{
	// GL_TRIANGLE_FAN is faster here,
	// but the circle is not as smooth
	// Depends on the look we want
	// Right now I prefer the smooth look
	glColor3f(r, g, b);
	
	glBegin(GL_LINES);
	for(int i = 0; i < 360; i++)
	{
		// float rad = _GUI_Radians(i);
		// float lenx = cos(rad) * radius;
		// float leny = sin(rad) * radius;

		float lenx = _GUI_SINCOS_LUT[i].c * radius;
		float leny = _GUI_SINCOS_LUT[i].s * radius;

		float px = x + lenx;
		float py = y + leny;

		glVertex2f(x, y);
		glVertex2f(px, py);
	}
	glEnd();
}

gui_rect _GUI_RectFromDimensions(float x, float y, float w, float h)
{
	gui_rect result;
	result.left = x;
	result.right = x + w;
	result.top = y;
	result.bottom = y + h;


	return result;
}

void GUI_Update(gui_context *Context, float mouse_x, float mouse_y, int window_w, int window_h, int lmb_pressed, int rmb_pressed)
{
	Context->mouse_x = mouse_x;
	Context->mouse_y = mouse_y;

	Context->window_w = window_w;
	Context->window_h = window_h;

	Context->lmb_pressed = lmb_pressed;
	Context->rmb_pressed = rmb_pressed;

	Context->element_is_active = false;
}

// 'a_min' and 'a_max' are expressed in terms of the space that 'a' is in
// 'b_min and 'b_max' express the desired space
float _GUI_LinearRemap(float a, float a_min, float a_max, float b_min, float b_max)
{
	float range_a = a_max - a_min;
	float range_b  = b_max - b_min;

	float p = (a - a_min) / range_a;
	float result = (p * range_b) + b_min;

	return result;
}

gui_v2f _GUI_MouseToWorld(gui_context *Context)
{
	// float px = (float)Context->mouse_x / (float)Context->window_w;
	// float py = (float)Context->mouse_y / (float)Context->window_y;

	// float mx = px * (float)Context->ortho_w;
	// float my = py * (float)Context->ortho-h;

	gui_v2f result;
	result.x = _GUI_LinearRemap(Context->mouse_x, 
			0, Context->window_w, 
			0, Context->ortho_w);

	result.y = _GUI_LinearRemap(Context->mouse_y, 
			0, Context->window_h, 
			0, Context->ortho_h);

	return result;
}


// value: starting value
// min: minimum allowable value
// max: maximum allowable value
// default_value: default value to reset to (currently on right click)
void GUI_SliderInit(gui_slider *Slider, float value, float min, float max, float default_value)
{
	GUI_ZERO_STRUCT(*Slider);
	Slider->value = value;
	Slider->transient_value = Slider->value;
	Slider->value_on_press = Slider->value;
	Slider->default_value = default_value;

	Slider->min = min;
	Slider->max = max;
}

void GUI_SliderBegin(gui_context *Context, gui_slider *Slider, float x, float y, float w, float h)
{
	// if(Context->rmb_pressed)
	{
		// Slider background
		// _GUI_Rect(x, y, w, h, 1, 1, 1, 0.5);
		_GUI_EntryPushRect(x, y, w, h, GUI_COLOR3F(1, 1, 1), 0.5);

		float half_height = h / 2.0;
		float quarter_height = h / 4.0;

		// Todo: Make a "CenteredRect" function
		// Slider bar
		float grey = 0.2;
		float slider_x = x;
		float slider_h = quarter_height;
		float slider_w = w;
		float slider_y = (y + half_height) - (slider_h / 2.0);
		// _GUI_Rect(slider_x, slider_y, slider_w, slider_h, grey, grey, grey, 1);
		_GUI_EntryPushRect(slider_x, slider_y, slider_w, slider_h, GUI_COLOR3F(grey, grey, grey), 1);

		// Circle
		float circle_y = y + (half_height);
		float diameter = half_height;
		float circle_radius = diameter * 0.5;

		// Account for circle radius
		float logical_x = x + circle_radius;
		float logical_w = w - diameter;

		// Note: Use 'logical_x' and 'logical_w' in place of 'x' and 'w' for all circle calculations
		float val = _GUI_LinearRemap(Slider->value_on_press, Slider->min, Slider->max, 0, 1);
		float circle_x = logical_x + (logical_w * val);
		float circle_visual_x = GUI_CLAMP(circle_x + Slider->mouse_dx, logical_x, logical_x + logical_w);

		gui_v2f world_mouse_pos = _GUI_MouseToWorld(Context);
		gui_rect CircleRect = _GUI_RectFromDimensions(circle_x - circle_radius, circle_y - circle_radius, diameter, diameter);
		
		gui_color3f circle_color;
		// Todo: Don't do this bounds check twice
		// "Hot" color
		if(_GUI_PointIsInsideRect(world_mouse_pos, &CircleRect) || Slider->active)
		{
			circle_color.r = 0.8;
			circle_color.g = 0.4;
			circle_color.b = 0.2;

			// If the mouse was not held when it entered the control
			if(!Context->lmb_pressed)
			{
				Slider->hot = true;
			}
		}
		// "Cold" color
		else
		{
			circle_color.r = 0.2;
			circle_color.g = 0.3;
			circle_color.b = 0.5;

			Slider->hot = false;
		}
		// _GUI_Circle(circle_visual_x, circle_y, circle_radius, circle_color.r, circle_color.g, circle_color.b);
		_GUI_EntryPushCircle(circle_visual_x, circle_y, circle_radius, GUI_COLOR3F(circle_color.r, circle_color.g, circle_color.b), 1);


		// Prevent the slider from being activated if an already-pressed mouse cursor is dragged over it
		// This might be undesirable behaviour in other circumstances but for Bunnymark this is what I want
		if(!Slider->active && Context->lmb_pressed && Slider->hot == false)
		{
			return;
		}



		// Now check for mouse hit
		// Todo: This mouse logic should go first, but we also need to calculate the desired dimensions before rendering
		if(_GUI_PointIsInsideRect(world_mouse_pos, &CircleRect))
		{
			// Press
			// Additional check for Slider->hot so the slider only
			// activates if the mouse was already over the button
			if(Context->lmb_pressed && !Context->rmb_pressed && !Slider->active && Context->active_elements == 0)
			{
				Slider->active = true;
				Slider->mouse_x_on_press = world_mouse_pos.x;
				Slider->transient_value = Slider->value;
				Slider->value_on_press = Slider->value;
				Slider->mouse_dx = 0;
				Context->element_is_active = true;
				Context->active_elements++;
			}
		}

		// Release
		if(!Context->lmb_pressed)
		{
			if(Slider->active)
			{
				Context->active_elements--;
			}

			Slider->active = false;
			Slider->value = Slider->transient_value;
			Slider->value_on_press = Slider->value;
			Slider->mouse_dx = 0;
			// Context->element_is_active = false;
		}
#if 1
		// Set the default value again
		// Really what this boils down to is a "hover" check
		// i.e. is the mouse hovering over the element?
		// if(Context->rmb_pressed && !Context->lmb_pressed)
		{
			// This encompasses the entire gui element
			// Only allow right click to register if it is initiated in the element
			gui_rect element_rect = _GUI_RectFromDimensions(x, y, w, h);
			if(_GUI_PointIsInsideRect(world_mouse_pos, &element_rect))
			{
				if(!Context->rmb_pressed)
				{
					Slider->hover = true;
				}
				else
				{
					// Todo: Allow right clicks from the user program to continue if entering from outside of the element
					Context->element_is_active = true;
					// Don't allow left click to override this action
					if(Slider->hover && !Context->lmb_pressed)
					{
						Slider->hover = false;
						Slider->transient_value = Slider->default_value;
						Slider->value = Slider->default_value;
						Slider->value_on_press = Slider->default_value;
					}
				}
			}
			else
			{
				Slider->hover = false;
			}
		}
		// else
		{
			// Context->element_is_active = false;
		}
#endif

		if(Slider->active)
		{
			float dx = world_mouse_pos.x - Slider->mouse_x_on_press;

			// Translate to box space
			float circle_midpoint = (logical_x + logical_w) - (circle_x + dx);
			circle_midpoint = GUI_CLAMP(circle_midpoint, 0, logical_w);

			// This tells us how far along we are in the slider in a 0-1 range
			float p = 1.0 - (circle_midpoint / logical_w);

			// Now remap that percentage into the user-defined value
			float v = _GUI_LinearRemap(p, 0, 1, Slider->min, Slider->max);

			// Save some state for rendering
			Slider->transient_value = v; 
			Slider->mouse_dx = dx;
		}
	}
}

void GUI_SliderEnd(gui_context *Context, gui_slider *Slider)
{
	// No op for now
}

double GUI_SliderGetValue(gui_slider *Slider)
{
	return Slider->transient_value;
}

bool GUI_SliderPressed(gui_slider *Slider)
{
	return Slider->active == true;
}

bool GUI_GuiElementActive(gui_context *Context)
{
	assert(Context->active_elements >= 0);
	return (Context->element_is_active == true 
		|| Context->active_elements > 0
		);
}

void GUI_FrameBegin(gui_context *Context)
{
	// No op for now
}

// New:
// Flushes and displays the elements
// Minimizes OpenGL state changes and defers rendering to a single function
// Note: Disables the user's shader. It's possible to call glGetIntegerv(GL_CURRENT_PROGRAM, &result)
// to cache the shader, but we don't do this at the moment
// It's the user's responsibility to re-enable any shaders that follow this call
void GUI_FrameEnd(gui_context *Context)
{
	gui_render_group *Group = &_GUI_RENDER_GROUP;

	glUseProgram(0);
	glLineWidth(1);
	glEnable(GL_LINE_SMOOTH);
	glDisable(GL_TEXTURE_2D);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0, Context->ortho_w, Context->ortho_h, 0, 0, 1);

	for(unsigned int i = 0; i < Group->count; i++)
	{
		gui_render_entry *Entry = &Group->Entries[i];
		switch(Entry->Type)
		{
			case GRE_RECT:
				_GUI_Rect(Entry->x, Entry->y, Entry->w, Entry->h, GUI_COLOR3F_TO_COMPONENTS(Entry->color), Entry->alpha);
			break;

			case GRE_CIRCLE:
				_GUI_Circle(Entry->x, Entry->y, Entry->radius, GUI_COLOR3F_TO_COMPONENTS(Entry->color));
			break;
		}
	}

	glPopMatrix();

	Group->count = 0;
}
