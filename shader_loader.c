char *ShaderOpen(const char *path)
{
	char *buffer = NULL;
	FILE *File = NULL;
	long size;
	
	File = fopen(path, "r");
	if(!File)
		LOADER_FATAL("ShaderOpen: Could not open file: %s\n", path);

	fseek(File, 0, SEEK_END);
	size = ftell(File);
	if(size == -1)
		LOADER_FATAL("LoadShader: ftell failed\n");
	else if(size == 0)
		LOADER_WARN("Empty file passed to load shader\n");

	
	buffer = calloc(size + 1, sizeof *buffer);
	if(!buffer)
		LOADER_FATAL("Could not allocate buffer\n");

	rewind(File);
	fread(buffer, 1, size, File);
	buffer[size] = 0;
	fclose(File);
	return buffer;
}

void ShaderCheck(GLuint shader, const char *file)
{
	int result;
	char msg[1024];

	glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
	if(!result)
	{

		fprintf(stderr, "For:\n"
				"%s:\n",
				file);
		glGetShaderInfoLog(shader, 1024, NULL, msg);
		fprintf(stderr, "%s\n", msg);
		exit(1);
	}
}
void ProgramCheck(GLuint program, const char *sourceVertex, const char *sourceFrag)
{
	int result;
	char msg[1024];

	glGetProgramiv(program, GL_LINK_STATUS, &result);
	if(!result)
	{
		fprintf(stderr, "For:\n"
				"%s:\n"
				"%s:\n",
				sourceVertex, sourceFrag);
		glGetProgramInfoLog(program, 1024, NULL, msg);
		fprintf(stderr, "%s\n", msg);
		exit(1);
	}
}



GLuint ShaderProgramCreate(const char *sourceVert, const char *sourceFrag)
{
	const char *dataVert;
	const char *dataFrag;
	GLuint shaderVert;
	GLuint shaderFrag;
	GLuint shaderProgram;

	/* Vertex shader */
	dataVert = ShaderOpen(sourceVert);
	shaderVert = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(shaderVert, 1, &dataVert, NULL);
	glCompileShader(shaderVert);
	ShaderCheck(shaderVert, sourceVert);

	/* Fragment shader */
	dataFrag = ShaderOpen(sourceFrag);
	shaderFrag = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(shaderFrag, 1, &dataFrag, NULL);
	glCompileShader(shaderFrag);
	ShaderCheck(shaderFrag, sourceFrag);

	/* Program */
	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, shaderVert);
	glAttachShader(shaderProgram, shaderFrag);
	glLinkProgram(shaderProgram);
	ProgramCheck(shaderProgram, sourceVert, sourceFrag);

	glDeleteShader(shaderVert);
	glDeleteShader(shaderFrag);

	free((void *)dataVert);
	free((void *)dataFrag);

	return shaderProgram;
}
