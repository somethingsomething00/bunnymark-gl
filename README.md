# Bunnymark GL

![thumbnail](./thumbnail.png)

```
How fast can your GPU really go? This benchmark tests your GPU by rendering hundreds of thousands of sprites.

Inspired by Farzher's Bunnymark DX in Jai.


Controls:
F......................Toggle Fullscreen
G......................Toggle HUD
Left Mouse Button......Spawn Bunnies
Right Mouse Button.....Despawn Bunnies
1......................Use OpenGL immediate mode for rendering
2......................Use OpenGL immediate mode for rendering (slightly faster implementation)
3......................Use OpenGL SSBO (fastest)

E......................Report timings to the console
R......................Clear timings


*Holding down LBM on the slider circle will move the slider 
*Holding down RMB on the slider will reset the slider to its default value


Building:
  ./build.sh
  ./bunnymark
  
Dependencies:
- 64 bit C compiler
- OpenGL 4.5 with support for Shader Storage Buffers
- GLFW3
- GLEW


Known bugs:
- The UI is still work in progress. Holding down the right mouse button over a UI element will disable it for the application.
- The calculation for gravity does not take delta time into account correctly.

```

## References
[Bunnymark DX Video](https://www.youtube.com/watch?v=_cSMGs2LyD8)

[Bunnymark DX Github](https://github.com/farzher/Bunnymark-Jai-D3D11)

[OpenGL Docs](https://docs.gl/)

[GLFW Docs](https://www.glfw.org/docs/latest/)

[stb_image](https://github.com/nothings/stb/blob/master/stb_image.h)

[stb_easy_font](https://github.com/nothings/stb/blob/master/stb_easy_font.h)

