#include <stdio.h>
#include <time.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef uint64_t u64;

#define PROFILER_STACK_SIZE 64

typedef struct
{
	const char *data;
	int len;
} str;

typedef struct
{
	str name;
	double secondsElapsed;
	u64 hits;
} timed_section;


typedef struct
{
	str name;
	u64 timestamp;
	int recordIndex;
} record;

timed_section TimedSections[PROFILER_STACK_SIZE] = {};
record Records[PROFILER_STACK_SIZE] = {};
record *ActiveRecords[PROFILER_STACK_SIZE] = {};
int ProfilerStackPointer = 0;
int ActiveProfilers = 0;

#if defined __linux__
u64 PerformanceCounter()
{
	struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);

	return ts.tv_nsec + (u64)ts.tv_sec * (u64)1e9;
}

double PerformanceFrequency()
{
	return 1e9;
}
#elif defined _WIN32
// Note: As of this time (2022-09), the Windows code has not been tested
u64 PerformanceCounter()
{
	LARGE_INTEGER Counter;
	QueryPerformanceCounter(&Counter);

	return Counter.QuadPart;
}

double PerformanceFrequency()
{
	static int Initialized = 0;
	static double Frequency;
	if(!Initialized)
	{
		LARGE_INTEGER I;
		Initialized = 1;
		QueryPerformanceFrequency(&I);
		Freq = (double)I.QuadPart;
	}
	return Frequency;
}
#else
#error "Performance counters for this OS are not implemented yet!"
#endif /* __linux__, _WIN32 */


void ProfileBegin(const char *name)
{
	assert(name != NULL);

	// Todo: Move this to a function
	// Todo: Make this a hash table lookup
	int recordIndex = -1;
	int namelen = strnlen(name, 255);
	int i = 0;
	for(;i < PROFILER_STACK_SIZE; i++)
	{
		record *R = &Records[i];
		if(!R->name.data)
		{
			R->name.data = name;
			R->name.len = namelen;
			recordIndex = i;
			ActiveProfilers++;
			break;
		}
		else
		{
			if(namelen != R->name.len)
			{
				continue;
			}
			
			int n = 0;
			while(n < namelen && name[n] == R->name.data[n])
			{
				n++;
			}
			if(n == namelen) 
			{
				recordIndex = i;
				break;
			}
		}
	}
	assert(recordIndex != -1);
	assert(i < PROFILER_STACK_SIZE);
	assert(ProfilerStackPointer < PROFILER_STACK_SIZE);

	ActiveRecords[ProfilerStackPointer] = &Records[recordIndex];
	record *Record = ActiveRecords[ProfilerStackPointer];
	
	Record->recordIndex = recordIndex;
	Record->timestamp = PerformanceCounter();

	ProfilerStackPointer++;
}

void ProfileEnd()
{
	assert(ProfilerStackPointer > 0);

	record *Record = ActiveRecords[--ProfilerStackPointer];
	u64 now = PerformanceCounter();
	u64 elapsed = now - Record->timestamp;
	int sectionIndex = Record->recordIndex;
	double seconds = (double)elapsed / PerformanceFrequency();

	TimedSections[sectionIndex].hits++;
	TimedSections[sectionIndex].secondsElapsed += seconds;
	TimedSections[sectionIndex].name = Record->name;
}

void ProfileClear()
{
	for(int i = 0; i < ActiveProfilers; i++)
	{
		timed_section *Ts = &TimedSections[i];
		Ts->secondsElapsed = 0;
		Ts->hits = 0;
	}
}

int ProfileGetMs(const char *name, double *ms)
{
	int found = 0;
	for(int i = 0; i < ActiveProfilers; i++)
	{
		timed_section *Ts = &TimedSections[i];
		int eq = strncmp(Ts->name.data, name, Ts->name.len) == 0;
		if(eq)
		{
			*ms = (Ts->secondsElapsed / (double)Ts->hits) * 1000.0;
			found = 1;
			break;
		}
	}
	return found;
}

void ProfileReport()
{
	double totalMs = 0;
	printf("===================================================\n");
	for(int i = 0; i < ActiveProfilers; i++)
	{
		timed_section *Ts = &TimedSections[i];
		double ms = (Ts->secondsElapsed / (double)Ts->hits) * 1000.0;
		totalMs += Ts->secondsElapsed;
		printf("%-15s %.4fms (%.4fs total)\n", Ts->name.data, ms, Ts->secondsElapsed);
	}

	totalMs *= 1000.0;
	printf("Total: %.4f ms\n", totalMs);

	printf("===================================================\n");
}

#ifdef __cplusplus
}
#endif
