#include <stdio.h>
#include <stdarg.h>

#include "stb_easy_font.h"

#define CON_ROWS_MAX 1000

typedef struct
{
	float r;
	float g;
	float b;
	float a;

	int vertCount;

} con_row_marker;

typedef struct
{
	float x;
	float y;
	float z;
	u8 color[4];
} easy_font_vert;

static float advance = 0;
#define EASY_FONT_VERT_BUFFER_SIZE (8192*2)
static easy_font_vert EasyFontVerts[EASY_FONT_VERT_BUFFER_SIZE];

static con_row_marker ConRowBuffer[CON_ROWS_MAX];

static int OrthoWidth = 640;
static int OrthoHeight = 480;
static int Rows = 0;
static u32 VertsUsed = 0;
static const float CON_ADVANCE = 12;


void CON_Init(int width, int height)
{
	OrthoWidth = width;
	OrthoHeight = height;
}

void CON_Begin()
{
	glUseProgram(0);

	glPushMatrix();
	glLoadIdentity();
	glOrtho(0, OrthoWidth, OrthoHeight, 0, 0, 1);
	glDisable(GL_TEXTURE_2D);

	advance = 0;
}

void CON_Print(float r, float g, float b, const char *s, ...)
{
	static char conBuffer[8192];
	va_list vl;
	va_start(vl, s);

	float x = 0;
	float y = advance;
	vsnprintf(conBuffer, 8192, s, vl);
	va_end(vl);

	easy_font_vert *VertPointer = &EasyFontVerts[VertsUsed];
	u32 BufferSize = sizeof(EasyFontVerts) - (VertsUsed * sizeof(EasyFontVerts[0]));
	int numQuads = stb_easy_font_print(x, y, conBuffer, 0, VertPointer, BufferSize);
	int vertCount = numQuads * 4;
	VertsUsed += vertCount;
	assert(VertsUsed < EASY_FONT_VERT_BUFFER_SIZE);
	
	advance += CON_ADVANCE;

	ConRowBuffer[Rows] = (con_row_marker){r, g, b, 1.0, vertCount};

	Rows++;
	assert(Rows < CON_ROWS_MAX);
}

void CON_Flush()
{
	glPushMatrix();
	float scale = 1.25;
	glScalef(scale, scale, 1);

	// Box for text
	float boxWidth = 150;
	float boxHeight = Rows * CON_ADVANCE;

	float gray  = 0.0;
	float a = 0.75;
	
	glColor4f(gray, gray, gray, a);
	
	glBegin(GL_QUADS);
	
	glVertex2f(0, 0);
	glVertex2f(0, boxHeight);
	glVertex2f(boxWidth, boxHeight);
	glVertex2f(boxWidth, 0);
	
	glEnd();


	// The text itself
	easy_font_vert *v = &EasyFontVerts[0];
	glBegin(GL_QUADS);
	glColor3f(1, 1, 1);
	for(int r = 0; r < Rows; r++)
	{
		con_row_marker *marker = &ConRowBuffer[r];
		glColor4f(marker->r, marker->g, marker->b, marker->a);
		for(int i = 0; i < marker->vertCount; i++)
		{
			glVertex2f(v[i].x, v[i].y);
		}
		v += marker->vertCount;
	}
	glEnd();

	glPopMatrix();
	
	VertsUsed = 0;
	Rows = 0;
}

void CON_End()
{
	glPopMatrix();
}
