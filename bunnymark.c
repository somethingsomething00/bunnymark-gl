/**********************************
* Libc
**********************************/
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <stdbool.h>


/**********************************
* GL
**********************************/
#include <GL/glew.h>
#include <GL/gl.h>
#include <GLFW/glfw3.h>


/**********************************
* Local
**********************************/
#include "types.h"
#include "profiler3.c"
#include "shader_loader.h"
#include "shader_loader.c"
#include "con.c"
#include "gui.c"

#define XS_IMPLEMENTATION
#include "xorshift.h"

#define STB_IMAGE_IMPLEMENTATION
#include "image.c"


// Our arena collision visuals are not exact because
// the uvs have some alpha space in the boundary
#define TEXTURE_WIDTH 430
#define TEXTURE_HEIGHT 36
#define TEX_U (36.0 / TEXTURE_WIDTH)
#define TEX_U_RIGHT (32.0 / TEXTURE_WIDTH)

#define BUNNY_WIDTH 32
#define BUNNY_HEIGHT 32
#define BUNNY_SPRITE_COUNT 12

#define SHADER_LOC_SSBO 1

#define GRAVITY_ENABLED 1
#define GRAVITY 38.0/10.0

#define PI 3.141592654

#define min(a, b) ((a) < (b)) ? (a) : (b)
#define max(a, b) ((a) > (b)) ? (a) : (b)
#define clamp(have, mi, ma) min(max((have), (mi)), (ma))


/**********************************
* Types
**********************************/

typedef struct
{
	float x;
	float y;

	// float vel_x;
	// float vel_y;

	// u32 spritenum;

	// u8 pad[12];
} bunny;

// Separate the data for more efficient transfer to the gpu
// This makes it slower to update on the cpu due to the data being separated,
// but the tradeoff is worth it
// Faster than doing a second memcpy anyhow
typedef struct
{
	float vel_x;
	float vel_y;
} bunny_delta;

typedef enum
{
	BUNNY_DRAW_IMPL_IMMEDIATE,
	BUNNY_DRAW_IMPL_IMMEDIATE_SMARTER,
	BUNNY_DRAW_IMPL_SSBO,
} bunny_draw_implementation;

typedef struct
{
	float x;
	float y;
} v2f;

typedef struct
{
	bunny *Bunnies;
	bunny_delta *BunnyDeltas;
	u32 used;
	u32 allocated;
	u64 bytes;
} bunny_buffer;

typedef struct
{
	int lmbHeld;
	int rmbHeld;
	float mouse_x;
	float mouse_y;

	int window_width;
	int window_height;

	int WasUpdated;

	bunny_draw_implementation DrawImplementation;

	// OpenGL 4.x stuff
	u32 ssbo;
	u32 vao;
	u32 ibo;
	u32 shader;

	u32 uOrtho;
	u32 uScale;

	u32 ArenaWidth;
	u32 ArenaHeight;

	float scale;

	u32 bunniesPerFrame;

	bool DisplayHud;

} bunnymark_context;


void GLFW_ToggleFullscreen(GLFWwindow *_Window);
static void CenterCursor(GLFWwindow *Window);


/**********************************
* Functions
**********************************/
const char *DrawImplementationString(bunny_draw_implementation impl)
{
#define DIS(i) case i: return #i
	switch(impl)
	{
		DIS(BUNNY_DRAW_IMPL_IMMEDIATE);
		DIS(BUNNY_DRAW_IMPL_IMMEDIATE_SMARTER);
		DIS(BUNNY_DRAW_IMPL_SSBO);
	}
	assert(0 && "Unhandled bunny draw implementation");
#undef DIS
}

const char *HumanReadableDrawImplementation(bunny_draw_implementation impl)
{
	switch(impl)
	{
		case BUNNY_DRAW_IMPL_IMMEDIATE: 
			return "Immediate";
		case BUNNY_DRAW_IMPL_IMMEDIATE_SMARTER: 
			return "Immediate V2";
		case BUNNY_DRAW_IMPL_SSBO:
			return "SSBO";
		default: 
			return "Unknown implementation";
	}

	assert(0);
}

GLuint SsboAlloc(u64 bytes, u32 shaderLocation)
{
	GLuint id;

	glGenBuffers(1, &id);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, id);
	glBufferData(GL_SHADER_STORAGE_BUFFER, bytes, 0, GL_DYNAMIC_DRAW);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, shaderLocation, id);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

	return id;
}

// Assumes ssbo is already bound
void SsboUpdate(void *data, u64 bytes)
{
	// glMapBuffer was too slow, glBufferSubData is faster
	glBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, bytes, data);
}

// Initialises the context for OpenGL 4.x rendering
// Expects to have an already allocated bunny buffer object for data calculations
bunnymark_context BunnymarkContextInit(bunny_buffer *bb, int window_width, int window_height, int ArenaWidth, int ArenaHeight)
{
	bunnymark_context Result = {0};

	Result.window_width = window_width;
	Result.window_height = window_height;
	Result.DrawImplementation = BUNNY_DRAW_IMPL_IMMEDIATE;
	Result.WasUpdated = 1;
	Result.ArenaWidth = ArenaWidth;
	Result.ArenaHeight = ArenaHeight;
	Result.scale = 1;
	Result.bunniesPerFrame = 100;
	Result.DisplayHud = true;

	// Since we're only drawing quads, we hardcode the number here
	u32 indexbufferCount = bb->allocated * 4;
	u32 *indexbuffer = calloc(indexbufferCount, sizeof *indexbuffer);
	u64 indexbufferSize = indexbufferCount * sizeof *indexbuffer;

	for(u32 i = 0; i < indexbufferCount; i++)
	{
		indexbuffer[i] = i;
	}

	glGenVertexArrays(1, &Result.vao);
	glBindVertexArray(Result.vao);

	glGenBuffers(1, &Result.ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Result.ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexbufferSize, indexbuffer, GL_STATIC_DRAW);

	glBindVertexArray(0);

	Result.shader = ShaderProgramCreate("ssbo.vert", "ssbo.frag");
	Result.uOrtho = glGetUniformLocation(Result.shader, "ortho");
	Result.uScale = glGetUniformLocation(Result.shader, "scale");

	// Note: The y component must be negative so that y=0 in world space is the top left
	glUseProgram(Result.shader);
	glUniform2f(Result.uOrtho, 2.0 / ((float)ArenaWidth), -2.0 / ((float)ArenaHeight));
	glUseProgram(0);

	Result.ssbo = SsboAlloc(bb->bytes, SHADER_LOC_SSBO);

	return Result;
}

/**********************************
* Globals
**********************************/
int running = 1;
xs_state32 random_state = {1234};
bunny_buffer BunnyBuffer = {0};
bunnymark_context BunnymarkContext = {0};


static v2f MouseToWorld()
{
	v2f result;

	float px = BunnymarkContext.mouse_x / (float)BunnymarkContext.window_width;
	float py = BunnymarkContext.mouse_y / (float)BunnymarkContext.window_height;

	result.x = px * BunnymarkContext.ArenaWidth;
	result.y = py * BunnymarkContext.ArenaHeight;

	return result;
}

void CbMouseButton(GLFWwindow *Window, int button, int action, int mod)
{
	int pressed = action == GLFW_PRESS;

	switch(button)
	{
		case GLFW_MOUSE_BUTTON_LEFT:
			BunnymarkContext.lmbHeld = pressed;
			break;
		case GLFW_MOUSE_BUTTON_RIGHT:
			BunnymarkContext.rmbHeld = pressed;
			break;
	}
}

void CbMousePos(GLFWwindow *Window, double x, double y)
{
	BunnymarkContext.mouse_x = x;
	BunnymarkContext.mouse_y = y;
}



void CbKeys(GLFWwindow *Window, int key, int scancode, int action, int mod)
{
#define KEY_PRESSED(k) if(key == k && action == GLFW_PRESS)

	KEY_PRESSED(GLFW_KEY_Q)
	{
		running = 0;
	}

	KEY_PRESSED(GLFW_KEY_ESCAPE)
	{
		running = 0;
	}

	KEY_PRESSED(GLFW_KEY_E)
	{
		ProfileReport();
	}

	KEY_PRESSED(GLFW_KEY_R)
	{
		ProfileClear();
	}

	KEY_PRESSED(GLFW_KEY_1)
	{
		BunnymarkContext.DrawImplementation = BUNNY_DRAW_IMPL_IMMEDIATE;
		BunnymarkContext.WasUpdated = 1;
	}

	KEY_PRESSED(GLFW_KEY_2)
	{
		BunnymarkContext.DrawImplementation = BUNNY_DRAW_IMPL_IMMEDIATE_SMARTER;
		BunnymarkContext.WasUpdated = 1;
	}

	KEY_PRESSED(GLFW_KEY_3)
	{
		BunnymarkContext.DrawImplementation = BUNNY_DRAW_IMPL_SSBO;
		BunnymarkContext.WasUpdated = 1;
	}

	KEY_PRESSED(GLFW_KEY_H)
	{
		BunnymarkContext.DisplayHud = !BunnymarkContext.DisplayHud;
	}

	KEY_PRESSED(GLFW_KEY_F)
	{
		GLFW_ToggleFullscreen(Window);
	}

	KEY_PRESSED(GLFW_KEY_L)
	{
		CenterCursor(Window);
	}

#undef KEY_PRESSED
}

void CbResize(GLFWwindow *Window, int w, int h)
{
	glViewport(0, 0, w, h);

	BunnymarkContext.window_width = w;
	BunnymarkContext.window_height = h;

	double mx, my;
	glfwGetCursorPos(Window, &mx, &my);
	BunnymarkContext.mouse_x = mx;
	BunnymarkContext.mouse_y = my;
}

void CbWindowClose(GLFWwindow *Window)
{
	running = 0;
}

float RandF32Range(float min, float max)
{
	return xs_range_in_f32(&random_state, min, max);
}

u32 Rand32Range(u32 min,  u32 max)
{
	return xs_range_ex_u32(&random_state, min, max);
}


float Radians(float degrees)
{
	return degrees * (PI / 180.0);
}

// The original bunnymark did not have sprite rotation,
// so I am not using is here either
// The rotation is only used to calculate velocity
void BunnyInit(bunny *Bunny, float x, float y)
{
	
	// float rotation = RandF32Range(0, 360);
	// float rad = Radians(rotation);
	
	// float vel_x = cos(rad) * speed;
	// float vel_y = sin(rad) * speed;

	// int spritenum = RandF32Range(0, BUNNY_SPRITE_COUNT);

	Bunny->x = x;
	Bunny->y = y;
	// Bunny->vel_x = vel_x;
	// Bunny->vel_y = vel_y;
	// Bunny->spritenum = spritenum;
}

void BunnyDeltaInit(bunny_delta *BunnyDelta)
{
	float speed = 300.12; // Was 5, now adjusted to be equivalent at 60 fps when multiplied by 0.0166
	// float speed = 500.12; // Was 5, now adjusted to be equivalent at 60 fps when multiplied by 0.0166

	float rotation = RandF32Range(0, 360);
	float rad = Radians(rotation);
#if GRAVITY_ENABLED
	rad = Radians(RandF32Range(180, 360));
	float yspeed = RandF32Range(1, 8);
	float vel_y = sin(rad) * yspeed;
#else
	float vel_y = sin(rad) * speed;
#endif

	float vel_x = cos(rad) * speed;
	
	BunnyDelta->vel_x = vel_x;
	BunnyDelta->vel_y = vel_y;
}

bunny_buffer BunniesAlloc(u32 count)
{
	bunny_buffer Result = {0};
	assert(count > 0);

	Result.bytes = count * sizeof *Result.Bunnies;
	Result.allocated = count;

	Result.Bunnies = calloc(count, sizeof *Result.Bunnies);
	assert(Result.Bunnies != NULL);

	Result.BunnyDeltas = calloc(count, sizeof *Result.BunnyDeltas);
	assert(Result.BunnyDeltas != NULL);

	return Result;
}

void BunniesAdd(bunny_buffer *bb, u32 count)
{
	u32 before = bb->used;
	bb->used = min(bb->used + count, bb->allocated);
	u32 after = bb->used;

	u32 bunniesToInit = after - before;

	v2f pos = MouseToWorld();
	// Since our origin is at the top left, to be centered we need to subtract
	// half the width and height
	pos.x -= ((BUNNY_WIDTH * BunnymarkContext.scale) / 2);
	pos.y -= ((BUNNY_HEIGHT * BunnymarkContext.scale) / 2);

	bunny *BunnyPtr = &bb->Bunnies[before];
	bunny_delta *DeltaPtr = &bb->BunnyDeltas[before];

	for(u32 i = 0; i < bunniesToInit; i++)
	{
		BunnyInit(BunnyPtr, pos.x, pos.y);
		BunnyDeltaInit(DeltaPtr);
		BunnyPtr++;
		DeltaPtr++;
	}
}

void BunniesRemove(bunny_buffer *bb, u32 count)
{
	if(bb->used >= count)
	{
		bb->used -= count;
	}
	else
	{
		bb->used = 0;
	}
}

int BunnyGetSpriteNum(int index)
{
	return (index / 1000) % 12;
}

// Temp
float _BunnyVisualWidth = BUNNY_WIDTH;
float _BunnyVisualHeight = BUNNY_HEIGHT;

void BunnyDrawImm(bunny *Bunny, int index)
{
	// float scale = BunnymarkContext.scale;

	float x0 = Bunny->x;
	float x1 = x0 + (_BunnyVisualWidth);

	float y0 = Bunny->y;
	float y1 = y0 + (_BunnyVisualHeight);

	int spritenum = BunnyGetSpriteNum(index);
	float u0 = spritenum * TEX_U;
	float u1 = u0 + TEX_U_RIGHT;

	float v0 = 0;
	float v1 = 1;

	glColor3f(1, 1, 1);

	glBegin(GL_QUADS);
	
	glTexCoord2f(u0, v0); glVertex2f(x0, y0);
	glTexCoord2f(u0, v1); glVertex2f(x0, y1);
	glTexCoord2f(u1, v1); glVertex2f(x1, y1);
	glTexCoord2f(u1, v0); glVertex2f(x1, y0);

	glEnd();
}

void BunnyDrawImmSmarter(bunny *Bunny, int index)
{
	float scale = BunnymarkContext.scale;

	float x0 = Bunny->x;
	float x1 = x0 + (BUNNY_WIDTH * scale);

	float y0 = Bunny->y;
	float y1 = y0 + (BUNNY_HEIGHT * scale);

	// float u0 = Bunny->spritenum * TEX_U;
	int spritenum = BunnyGetSpriteNum(index);
	float u0 = spritenum * TEX_U;
	float u1 = u0 + TEX_U_RIGHT;

	float v0 = 0;
	float v1 = 1;

	glTexCoord2f(u0, v0); glVertex2f(x0, y0);
	glTexCoord2f(u0, v1); glVertex2f(x0, y1);
	glTexCoord2f(u1, v1); glVertex2f(x1, y1);
	glTexCoord2f(u1, v0); glVertex2f(x1, y0);
}


void BunnyUpdate(bunny *Bunny, bunny_delta *BunnyDelta, float dtSec)
{
	float dx = BunnyDelta->vel_x * dtSec;

#if GRAVITY_ENABLED
	BunnyDelta->vel_y += GRAVITY * dtSec;
	float dy = BunnyDelta->vel_y;
#else
	float dy = BunnyDelta->vel_y * dtSec;
#endif

	Bunny->x += dx;
	Bunny->y += dy;

	float x_min = 0;
	float x_max = BunnymarkContext.ArenaWidth - (_BunnyVisualWidth);
	float y_min = 0;
	float y_max = BunnymarkContext.ArenaHeight - (_BunnyVisualHeight);

	if(Bunny->x < x_min || Bunny->x >= x_max)
	{
		BunnyDelta->vel_x *= -1;
		Bunny->x = clamp(Bunny->x, x_min, x_max);
	}

#if GRAVITY_ENABLED
	if(Bunny->y >= y_max)
	{
		BunnyDelta->vel_y = RandF32Range(-8, -10);
		BunnyDelta->vel_y += RandF32Range(-1, 0.1);

		Bunny->y = clamp(Bunny->y, y_min, y_max);
	}
#else
	if(Bunny->y <= y_min || Bunny->y >= y_max)
	{
		BunnyDelta->vel_y *= -1;
		Bunny->y = clamp(Bunny->y, y_min, y_max);
	}
#endif
}

void BunniesUpdate(bunny_buffer *bb, float dtSec)
{
	// Temp
	_BunnyVisualWidth = BUNNY_WIDTH * BunnymarkContext.scale;
	_BunnyVisualHeight = BUNNY_HEIGHT * BunnymarkContext.scale;
	for(u32 i = 0; i < bb->used; i++)
	{
		bunny *Bunny = &bb->Bunnies[i];
		bunny_delta *Delta = &bb->BunnyDeltas[i];
		BunnyUpdate(Bunny, Delta, dtSec);
	}
}

void BunniesRender(bunny_buffer *bb, bunnymark_context *bmc)
{
	switch(bmc->DrawImplementation)
	{
		// glBegin / glEnd for every call
		case BUNNY_DRAW_IMPL_IMMEDIATE:
		{
			for(u32 i = 0; i < bb->used; i++)
			{
				bunny *Bunny = &bb->Bunnies[i];
				BunnyDrawImm(Bunny, i);
			}
		}
		break;

		case BUNNY_DRAW_IMPL_IMMEDIATE_SMARTER:
		{
			// glBegin at the start
			// glEnd at the end
			glColor3f(1, 1, 1);
			glBegin(GL_QUADS);
			for(u32 i = 0; i < bb->used; i++)
			{
				bunny *Bunny = &bb->Bunnies[i];
				BunnyDrawImmSmarter(Bunny, i);
			}
			glEnd();
		}
		break;

		case BUNNY_DRAW_IMPL_SSBO:
		{
			glUseProgram(bmc->shader);
			glBindVertexArray(BunnymarkContext.vao);
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, bmc->ssbo);
			
			SsboUpdate(bb->Bunnies, bb->used * sizeof *bb->Bunnies);
			glDrawElements(GL_QUADS, bb->used * 4, GL_UNSIGNED_INT, 0);

			// This works too, and we don't even need an index buffer
			// However, core OpenGL deprecates GL_QUADS in which case we do need
			// an index buffer to get the correct gl_VertexID number
			// glDrawArrays(GL_QUADS, 0, bb->used * 4);

			glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
			glBindVertexArray(0);
			glUseProgram(0);
		}
		break;
	}

}

GLuint TEX_AllocFromImage(image *Image, GLuint filter, GLuint wrap)
{
	GLuint id;
	glGenTextures(1, &id);
	glBindTexture(GL_TEXTURE_2D, id);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, Image->width, Image->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, Image->data);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap);

	glBindTexture(GL_TEXTURE_2D, 0);

	return id;
}

// Specific hack for this program
void BunniesProcessInput(bunnymark_context *bmc, bool guiActive)
{
	if(guiActive) return;

	u32 count = bmc->bunniesPerFrame;
	if(bmc->lmbHeld)
	{
		BunniesAdd(&BunnyBuffer, count);
		bmc->WasUpdated = 1;
	}

	if(bmc->rmbHeld)
	{
		BunniesRemove(&BunnyBuffer, count);
		bmc->WasUpdated = 1;
	}
}

void ContextSetScale(bunnymark_context *bmc, float scale)
{
	float lastScale = bmc->scale;
	bmc->scale = scale;

	if(bmc->scale != lastScale)
	{
		glUseProgram(bmc->shader);
		glUniform1f(bmc->uScale, bmc->scale);
		glUseProgram(0);
	}
}

void GLFW_ToggleFullscreen(GLFWwindow *_Window)
{
	struct windowplacement
	{
		int x;
		int y;
		int w;
		int h;
	};

	static struct windowplacement ws;
	static int initialized = 0;

	GLFWmonitor *_WindowMonitor = glfwGetWindowMonitor(_Window);
	const GLFWvidmode *_Vidmode = NULL;

	// Currently fullscreen, set to windowed
	if(_WindowMonitor)
	{
		// Init windowplacement struct
		if(!initialized)
		{
			_Vidmode = glfwGetVideoMode(_WindowMonitor);
			ws.w = _Vidmode->width / 2;
			ws.h = _Vidmode->height / 2;

			ws.x = (_Vidmode->width / 2) - (ws.w / 2);
			ws.y = (_Vidmode->height / 2) - (ws.h / 2);
		}

		// Maybe we should set the refresh rate explicitly
		glfwSetWindowMonitor(_Window, 0, ws.x, ws.y, ws.w, ws.h, GLFW_DONT_CARE);
	}
	// Currently windowed, set to fullscreen
	else
	{
		// Cache window placement
		glfwGetWindowPos(_Window, &ws.x, &ws.y);
		glfwGetWindowSize(_Window, &ws.w, &ws.h);

		// Fullscreen dimensions
		GLFWmonitor *_PrimaryMonitor = glfwGetPrimaryMonitor();
		_Vidmode = glfwGetVideoMode(_PrimaryMonitor);
		glfwSetWindowMonitor(_Window, _PrimaryMonitor, 0, 0, _Vidmode->width, _Vidmode->height, GLFW_DONT_CARE);

		// Hack
		// If we begin in windowed mode, this counts as an initialization
		initialized = 1;
	}
}

// For the screenshot
static void CenterCursor(GLFWwindow *Window)
{
	double mx, my;
	int fbx, fby;
	double mxDesired;
	double myDesired;

	glfwGetFramebufferSize(Window, &fbx, &fby);
	glfwGetCursorPos(Window, &mx, &my);

	mxDesired = (float)fbx / 2;
	myDesired = my;

	glfwSetCursorPos(Window, mxDesired, myDesired);
	BunnymarkContext.mouse_x = mxDesired;
	BunnymarkContext.mouse_y = myDesired;
}

int main()
{
	glfwInit();
	GLFWwindow *Window = glfwCreateWindow(800, 600, "Bunnymark GL", 0, 0);
	glfwMakeContextCurrent(Window);
	glewInit(); /* Important!!! This is how OpenGL 4.x functions are loaded */

	glfwSetKeyCallback(Window, CbKeys);
	glfwSetFramebufferSizeCallback(Window, CbResize);
	glfwSetMouseButtonCallback(Window, CbMouseButton);
	glfwSetCursorPosCallback(Window, CbMousePos);
	glfwSetWindowCloseCallback(Window, CbWindowClose);

	image BunnyAtlas = IMAGE_Load32("bunnies.png");
	image BunnyCursor = IMAGE_Load32("bunny.png");
	assert(BunnyAtlas.width == TEXTURE_WIDTH);
	assert(BunnyAtlas.height == TEXTURE_HEIGHT);
	GLuint TexBunny = TEX_AllocFromImage(&BunnyAtlas, GL_NEAREST, GL_CLAMP);
	IMAGE_Free(&BunnyAtlas);

	GLFWimage GlfwImage;
	GlfwImage.width = BunnyCursor.width;
	GlfwImage.height = BunnyCursor.height;
	GlfwImage.pixels = BunnyCursor.data;
	GLFWcursor *Cursor = glfwCreateCursor(&GlfwImage, 0, 0);
	glfwSetCursor(Window, Cursor);
	IMAGE_Free(&BunnyCursor);


	BunnyBuffer = BunniesAlloc(2e6);
	{
		int w, h;
		glfwGetFramebufferSize(Window, &w, &h);
		BunnymarkContext = BunnymarkContextInit(&BunnyBuffer, w, h, 4000, 4000);
	}

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glOrtho(0, BunnymarkContext.ArenaWidth, BunnymarkContext.ArenaHeight, 0, 0, 1);

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, TexBunny);
	
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	glClearColor(0, 0, 0, 0);

	double deltaSec = 0.0166;

	ContextSetScale(&BunnymarkContext, 0.5);
	double TimeToUpdateSec = 0;

	CON_Init(640, 480);

	gui_context GuiContext;
	gui_slider ScaleSlider;
	gui_slider BunnyCountSlider;
	GUI_Init(&GuiContext, 640, 480);
	GUI_SliderInit(&ScaleSlider, 1.0, 0.1, 10, 1);
	GUI_SliderInit(&BunnyCountSlider, 100, 1, 1000, 100);

	while(running)
	{
		ProfileBegin("Frame");
		double t1 = glfwGetTime();
		glClear(GL_COLOR_BUFFER_BIT);

	
		GUI_Update(&GuiContext,
				BunnymarkContext.mouse_x, BunnymarkContext.mouse_y,
				BunnymarkContext.window_width, BunnymarkContext.window_height,
				BunnymarkContext.lmbHeld, BunnymarkContext.rmbHeld);

		GUI_FrameBegin(&GuiContext);


		// Update
		ProfileBegin("Bunny Update");
		BunniesUpdate(&BunnyBuffer, deltaSec);
		ProfileEnd();
		
		// Render
		glEnable(GL_TEXTURE_2D);
		ProfileBegin("Bunny Render");
		BunniesRender(&BunnyBuffer, &BunnymarkContext);
		ProfileEnd();

		if(BunnymarkContext.WasUpdated)
		{
			BunnymarkContext.WasUpdated = 0;
			char title[256];
			snprintf(title, 256, "Bunnymark GL [%u] [%s]", 
				BunnyBuffer.used, 
				DrawImplementationString(BunnymarkContext.DrawImplementation));

			glfwSetWindowTitle(Window, title);
		}
		if(BunnymarkContext.DisplayHud)
		{
			double msRender = -1;
			double msUpdate = -1;
			double msFrame = -1;
			ProfileGetMs("Bunny Update", &msUpdate);
			ProfileGetMs("Bunny Render", &msRender);
			ProfileGetMs("Frame", &msFrame);
			CON_Begin();
			CON_Print(1, 1, 1, "Bunnies: %u\n", BunnyBuffer.used);
			CON_Print(1, 1, 1, "Bunnies per frame: %u\n", BunnymarkContext.bunniesPerFrame);
			CON_Print(1, 1, 1, "Sprite scale: %.2f", BunnymarkContext.scale);
			CON_Print(1, 1, 1, "Method: %s", HumanReadableDrawImplementation(BunnymarkContext.DrawImplementation));
			CON_Print(1, 1, 1, "Update time: %.2f ms", msUpdate);
			CON_Print(1, 1, 1, "Render time: %.2f ms", msRender);
			CON_Print(1, 1, 1, "FPS (frame):  %.2f", 1000.0 / msFrame);
			CON_Print(1, 1, 1, "FPS (render): %.2f", 1000.0 / msRender);
			CON_Flush();
			CON_End();


			ProfileBegin("GUI Update");
			float guiX = 120;
			// Input
			// We process input at the end due to us begin dependant on the slider value
			GUI_SliderBegin(&GuiContext, &ScaleSlider, 10, guiX, 160, 20);
			GUI_SliderEnd(&GuiContext, &ScaleSlider);

			GUI_SliderBegin(&GuiContext, &BunnyCountSlider, 10, guiX + 30, 160, 20);
			GUI_SliderEnd(&GuiContext, &BunnyCountSlider);

			ProfileEnd();
		}
		ProfileBegin("GUI Render");
		GUI_FrameEnd(&GuiContext);
		ProfileEnd();

		float scale = GUI_SliderGetValue(&ScaleSlider);
		u32 bunniesPerFrame = GUI_SliderGetValue(&BunnyCountSlider);
		BunnymarkContext.bunniesPerFrame = bunniesPerFrame;
		ContextSetScale(&BunnymarkContext, scale);
		BunniesProcessInput(&BunnymarkContext, GUI_GuiElementActive(&GuiContext));

		
		glfwSwapBuffers(Window);
		glfwPollEvents();

		ProfileEnd();

		double t2 = glfwGetTime();
		deltaSec = (t2 - t1);

		TimeToUpdateSec += deltaSec;
		if(TimeToUpdateSec >= 5.0)
		{
			TimeToUpdateSec = 0;
			ProfileClear();
		}
	}
}
