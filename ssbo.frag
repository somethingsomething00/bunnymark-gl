#version 430

uniform sampler2D TEX0;

in vec2 vf_TexCoords;
in float vf_Idx;
out vec4 FragColor;
void main()
{
	vec4 TexColor = texture(TEX0, vf_TexCoords);
	// This won't match the immediate-mode GL renderer but it looks a lot better
	if(TexColor.a < 0.75) discard;

	FragColor = TexColor;
}
